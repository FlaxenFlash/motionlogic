namespace Assets.Source.Animation.InputProcessing
{
	public interface IEndPoint
	{
		void TriggerEndPoint();
	}
}