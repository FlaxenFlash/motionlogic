using System.Collections.Generic;
using Assets.Source.Animation.InputProcessing;
using BitStrap;
using UnityEditor;
using UnityEngine;

namespace Assets.Source.Animation.AnimatorProcessing
{
	public class AnimatorParameterProcessor : MonoBehaviour
	{
		private List<AnimatorProcessingUnit> _processors;

		public Animator Animator;
		public bool OverrideDestinationAnimator;
		[ConditionalHide("OverrideDestinationAnimator",true)]public Animator DestinationAnimator;
		public List<AnimatorProcessingScheme> Processors;

		public void Start()
		{
			_processors = new List<AnimatorProcessingUnit>();
			foreach (var scheme in Processors)
			{
				var processor = scheme.CreateProcessingUnit();
				_processors.Add(processor);
			}
		}

		public void Update()
		{
			foreach (var processor in _processors)
			{
				processor.Update(Animator, OverrideDestinationAnimator ? DestinationAnimator : Animator);
			}
		}

		[Button]
		public void Restart()
		{
			Start();
		}

#if UNITY_EDITOR
	    [Button]
	    public void CreateNewProcessor()
	    {
            var path = EditorUtility.SaveFilePanel(
                    "Create new Processing Scheme",
                    Application.dataPath,
                    "newScheme.asset",
                    "asset");

	        if (path.Length == 0) return;

            var processor = ScriptableObject.CreateInstance<AnimatorProcessingScheme>();

            AssetDatabase.CreateAsset(processor, "Assets" + path.Substring(Application.dataPath.Length));

            AssetDatabase.SaveAssets();

            Processors.Add(processor);
	    }
#endif
	}
}