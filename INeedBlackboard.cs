namespace Assets.Source.Animation.InputProcessing
{
	public interface INeedBlackboard
	{
		void GiveBlackboardAccess(IBlackboardAccess access);
		void Update();
	}
}