namespace Assets.Source.Animation.InputProcessing
{
	public interface INeedAnimator
	{
		void GiveAnimatorAccess(IAnimatorAccess access);
	}
}