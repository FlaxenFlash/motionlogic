using System;
using Assets.Source.Animation.InputProcessing;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing.Smoothing
{
	[Serializable]
	public class SmoothDampValue : Node, INeedBlackboard
	{
		private IBlackboardAccess _blackboard;
		private Guid _parameter;
		private Guid _previousSpeed;

		[Input]
		public float Target;
		[Output]
		public float Output;

		public float DampingValue = 1f;
		public float DescentMultiplier = 1f;
		public float MaxSpeed = float.PositiveInfinity;
		

		public void GiveBlackboardAccess(IBlackboardAccess access)
		{
			_blackboard = access;
			if (_parameter == Guid.Empty) _parameter = Guid.NewGuid();
			if (_previousSpeed == Guid.Empty) _previousSpeed = Guid.NewGuid();
		}

		public void Update()
		{
			if (Time.deltaTime == 0f) return;
			var previous = _blackboard.GetValue(_parameter);
			var speed = _blackboard.GetValue(_previousSpeed);
			var target = GetInputValueAsFloat("Target", Target);

			var direction = Mathf.Sign(target - previous);
			var damping = DampingValue * (direction < 0 ? DescentMultiplier : 1);
			var value = Mathf.SmoothDamp(previous, target, ref speed, damping, MaxSpeed);

			_blackboard.SetValue(_previousSpeed, speed);
			_blackboard.SetValue(_parameter, value);
		}

		public override float GetValueAsFloat(NodePort port)
		{
			return _blackboard.GetValue(_parameter);
		}
	}
}