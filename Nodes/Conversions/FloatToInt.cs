﻿using System;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing.Conversions
{
	[Serializable]
	public class FloatToInt : Node
	{
		[Input] public float Value;
		[Output] public int Output;

		public override int GetValueAsInt(NodePort port)
		{
			return Mathf.RoundToInt(GetInputValueAsFloat("Value", Value));
		}
	}
}
