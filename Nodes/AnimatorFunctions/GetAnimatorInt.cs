using System;
using Assets.Source.Animation.InputProcessing;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing.AnimatorFunctions
{
	[Serializable]
	public class GetAnimatorInt : Node, INeedAnimator
	{
		private Animator _animator;

		public string ParameterName;
		public int DefaultValue;
		[Output] public int Output;
		[Output] public float AsFloat;

		public override int GetValueAsInt(NodePort port)
		{
			var value = _animator == null ? DefaultValue : _animator.GetInteger(ParameterName);
			return value;
		}

		public override float GetValueAsFloat(NodePort port)
		{
			return GetValueAsInt(port);
		}

		public void GiveAnimatorAccess(IAnimatorAccess access)
		{
			_animator = access.GetSourceAnimator();
		}
	}
}