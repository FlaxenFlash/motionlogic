﻿using System;
using Assets.Source.Animation.InputProcessing;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing.AnimatorFunctions
{
	[Serializable]
	public class SetAnimatorBool : Node, INeedAnimator, IEndPoint
	{
		private Animator _animator;

		public string ParameterName;
		[Input]
		public bool Value;
		[Output]
		public bool Output;

		public override bool GetValueAsBool(NodePort port)
		{
			return GetInputValueAsBool("Value", Value);
		}

		public void GiveAnimatorAccess(IAnimatorAccess access)
		{
			_animator = access.GetDestinationAnimator();
		}

		public void TriggerEndPoint()
		{
			if (String.IsNullOrEmpty(ParameterName)) return;
			_animator.SetBool(ParameterName, GetInputValueAsBool("Value", Value));
		}
	}
}