﻿using System;
using Assets.Source.Animation.InputProcessing;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing.AnimatorFunctions
{
	[Serializable]
	public class GetAnimatorBool : Node, INeedAnimator
	{
		private Animator _animator;

		public string ParameterName;
		public bool DefaultValue;
		[Output] public bool Output;
		[Output] public float AsFloat;

		public override float GetValueAsFloat(NodePort port)
		{
			return GetValueAsBool(port) ? 1f : 0f;
		}

		public override bool GetValueAsBool(NodePort port)
		{
			var value = _animator == null ? DefaultValue : _animator.GetBool(ParameterName);
			return value;
		}

		public void GiveAnimatorAccess(IAnimatorAccess access)
		{
			_animator = access.GetSourceAnimator();
		}
	}
}