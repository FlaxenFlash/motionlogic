﻿using System;
using Assets.Source.Animation.InputProcessing;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing.AnimatorFunctions
{
	[Serializable]
	public class SetAnimatorFloat : Node, INeedAnimator, IEndPoint
	{
		private Animator _animator;

		public string ParameterName;
		[Input]
		public float Value;
		[Output]
		public float Output;

		public override float GetValueAsFloat(NodePort port)
		{
			return GetInputValueAsFloat("Value", Value);
		}

		public void GiveAnimatorAccess(IAnimatorAccess access)
		{
			_animator = access.GetDestinationAnimator();
		}

		public void TriggerEndPoint()
		{
			if (String.IsNullOrEmpty(ParameterName)) return;
			_animator.SetFloat(ParameterName, GetInputValueAsFloat("Value", Value));
		}
	}
}