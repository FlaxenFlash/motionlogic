﻿using System;
using Assets.Source.Animation.InputProcessing;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing.AnimatorFunctions
{
	[Serializable]
	public class GetAnimatorFloat : Node, INeedAnimator
	{
		private Animator _animator;

		public string ParameterName;
		public float DefaultValue;
		[Output]
		public float Output;

		public override float GetValueAsFloat(NodePort port)
		{
			return _animator == null ? DefaultValue : _animator.GetFloat(ParameterName);
		}

		public void GiveAnimatorAccess(IAnimatorAccess access)
		{
			_animator = access.GetSourceAnimator();
		}
	}
}
