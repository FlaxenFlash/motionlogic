﻿using System;
using Assets.Source.Animation.InputProcessing;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing.AnimatorFunctions
{
	[Serializable]
	public class SetAnimatorInt : Node, INeedAnimator, IEndPoint
	{
		private Animator _animator;

		public string ParameterName;
		[Input] public int Value;
		[Output] public int Output;

		public override int GetValueAsInt(NodePort port)
		{
			return GetInputValueAsInt("Value", Value);
		}

		public void GiveAnimatorAccess(IAnimatorAccess access)
		{
			_animator = access.GetDestinationAnimator();
		}

		public void TriggerEndPoint()
		{
			if (String.IsNullOrEmpty(ParameterName)) return;
			_animator.SetInteger(ParameterName, GetInputValueAsInt("Value", Value));
		}
	}
}