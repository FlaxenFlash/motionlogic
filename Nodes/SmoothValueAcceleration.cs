using System;
using Assets.Source.Animation.InputProcessing;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing.Smoothing
{
	[Serializable]
	public class SmoothValueAcceleration : Node, INeedBlackboard
	{
		private IBlackboardAccess _blackboard;
		private Guid _parameter;
		private Guid _previousSpeed;

		[Input] public float Target;
		[Output] public float Output;

		public float MaxAccelerationPerSecond = 1;
		public float SmoothingDescentMultiplier = 1;

		public void GiveBlackboardAccess(IBlackboardAccess access)
		{
			_blackboard = access;
			if (_parameter == Guid.Empty) _parameter = Guid.NewGuid();
			if (_previousSpeed == Guid.Empty) _previousSpeed = Guid.NewGuid();
		}

		public void Update()
		{
			if (Time.deltaTime == 0f) return;
			var previous = _blackboard.GetValue(_parameter);
			var previousSpeed = _blackboard.GetValue(_previousSpeed);
			var target = GetInputValueAsFloat("Target", Target);

			var direction = Mathf.Sign(target - previous);
			var directionMultiplier = direction < 0 ? SmoothingDescentMultiplier : 1;
			var change = Mathf.Abs(target - previous);
			change = Mathf.Min(change,(previousSpeed + MaxAccelerationPerSecond * directionMultiplier * Time.deltaTime) * Time.deltaTime);
			_blackboard.SetValue(_previousSpeed, change / Time.deltaTime);
			_blackboard.SetValue(_parameter, Mathf.MoveTowards(previous, target, change));
		}

		public override float GetValueAsFloat(NodePort port)
		{
			return _blackboard.GetValue(_parameter);
		}
	}
}