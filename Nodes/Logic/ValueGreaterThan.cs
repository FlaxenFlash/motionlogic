using System;
using Siccity.XNode;

namespace Flax.AnimatorProcessing.Logic
{
	[Serializable]
	public class ValueGreaterThan : Node
	{
		[Input] public float Value;
		[Input] public float Threshold;
		[Output] public bool Output;
		[Output] public float AsFloat;

		public bool OrEqualTo;

		public override bool GetValueAsBool(NodePort port)
		{
			return GetValueInternal();
		}

		public override float GetValueAsFloat(NodePort port)
		{
			return GetValueInternal() ? 1f : 0f;
		}

		private bool GetValueInternal()
		{
			var iValue = GetInputValueAsFloat("Value", Value);
			var iThreshold = GetInputValueAsFloat("Threshold", Threshold);
			var value = OrEqualTo ? iValue >= iThreshold : iValue > iThreshold;
			return value;
		}
	}
}