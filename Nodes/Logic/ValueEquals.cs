using System;
using Assets.Source.Tools;
using Siccity.XNode;

namespace Flax.AnimatorProcessing.Logic
{
	[Serializable]
	public class ValueEquals : Node
	{
		[Input] public float Value;
		[Input] public float Threshold;
		[Output] public bool Output;
		[Output] public float AsFloat;

		public bool Approx = true;

		public override float GetValueAsFloat(NodePort port)
		{
			var value = GetValueInternal();
			return value ? 1f : 0f;
		}

		public override bool GetValueAsBool(NodePort port)
		{
			return GetValueInternal();
		}

		private bool GetValueInternal()
		{
			var inputValue = GetInputValueAsFloat("Value", Value);
			var threshold = GetInputValueAsFloat("Threshold", Threshold);
			var value = Approx ? inputValue.ApproximatelyEqualTo(threshold) : inputValue == threshold;
			return value;
		}
	}
}