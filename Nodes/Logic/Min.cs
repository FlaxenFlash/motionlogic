using System;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing.Logic
{
	[Serializable]
	public class Min : Node
	{
		[Input] public float A;
		[Input] public float B;
		[Output] public float Output;

		public override float GetValueAsFloat(NodePort port)
		{
			var a = GetInputValueAsFloat("A", A);
			var b = GetInputValueAsFloat("B", B);
			var value = Mathf.Min(a,b);
			return value;
		}
	}
}