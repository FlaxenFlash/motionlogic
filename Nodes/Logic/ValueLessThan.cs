using System;
using Siccity.XNode;

namespace Flax.AnimatorProcessing.Logic
{
	[Serializable]
	public class ValueLessThan : Node
	{
		[Input] public float Value;
		[Input] public float Threshold;
		[Output] public bool Output;
		[Output] public float AsFloat;

		public bool OrEqualTo;

		public override bool GetValueAsBool(NodePort port)
		{
			var inputValue = GetInputValueAsFloat("Value", Value);
			var threshold = GetInputValueAsFloat("Threshold", Threshold);
			var value = OrEqualTo ? inputValue <= threshold : inputValue < threshold;

			return value;
		}

		public override float GetValueAsFloat(NodePort port)
		{
			return GetValueAsBool(port) ? 1f : 0f;
		}
	}
}