﻿using System;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing
{
	[Serializable]
	public class NonLinearResponse : Node
	{
		[Input] public float Value;
		[Output] public float Output;

		public AnimationCurve ResponseCurve = AnimationCurve.Linear(0,0,1,1);
		public bool ResponseIgnoreSign = true;

		public override float GetValueAsFloat(NodePort port)
		{
			var value = GetInputValueAsFloat("Value", Value);
			var sign = Mathf.Sign(value);
			return (ResponseIgnoreSign ? sign : 1) * ResponseCurve.Evaluate(ResponseIgnoreSign ? Mathf.Abs(value) : value);
		}
	}
}