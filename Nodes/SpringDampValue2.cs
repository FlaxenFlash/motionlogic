using System;
using Assets.Source.Animation.InputProcessing;
using Siccity.XNode;
using UnityEngine;

//Implemented based on suggestions found here https://www.gamedev.net/articles/programming/math-and-physics/towards-a-simpler-stiffer-and-more-stable-spring-r3227/?tab=comments
namespace Flax.AnimatorProcessing.Smoothing
{
	[Serializable]
	public class SpringDampValue2 : Node, INeedBlackboard
	{
		private IBlackboardAccess _blackboard;
		private Guid _value;
		private Guid _speed;

		[Input]
		public float Target;
		[Output]
		public float Output;

		[Range(0,1)]
		public float SpringStrength = 1f;
		[Range(0,1)]
		public float Damping = 1f;
		

		public void GiveBlackboardAccess(IBlackboardAccess access)
		{
			_blackboard = access;
			if (_value == Guid.Empty) _value = Guid.NewGuid();
			if (_speed == Guid.Empty) _speed = Guid.NewGuid();
		}

		public void Update()
		{
			if (Time.deltaTime == 0f) return;
			var previous = _blackboard.GetValue(_value);
			var speed = _blackboard.GetValue(_speed);

			var distance = GetInputValueAsFloat("Target", Target) - previous;
			var dampingMax = 1/Time.deltaTime;
			var springMax = dampingMax/Time.deltaTime;
			var force = springMax*SpringStrength*distance - dampingMax*Damping*speed;
			speed += force*Time.deltaTime;
			var value = previous + speed*Time.deltaTime;

			_blackboard.SetValue(_speed, speed);
			_blackboard.SetValue(_value, value);
		}

		public override float GetValueAsFloat(NodePort port)
		{
			return _blackboard.GetValue(_value);
		}
	}
}