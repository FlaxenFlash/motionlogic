using System;
using Assets.Source.Animation.InputProcessing;
using Assets.Source.Tools;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing.Smoothing
{
	[Serializable]
	public class SpringDampValue : Node, INeedBlackboard
	{
		private IBlackboardAccess _blackboard;
		private Guid _value;
		private Guid _speed;

		[Input]
		public float Target;
		[Output]
		public float Output;

		public float SpringStrength = 1f;
		public float Damping = 1f;
		public float MaxSpeed = 100f;
		

		public void GiveBlackboardAccess(IBlackboardAccess access)
		{
			_blackboard = access;
			if (_value == Guid.Empty) _value = Guid.NewGuid();
			if (_speed == Guid.Empty) _speed = Guid.NewGuid();
		}

		public void Update()
		{
			if (Time.deltaTime == 0f) return;
			var previous = _blackboard.GetValue(_value);
			var speed = _blackboard.GetValue(_speed);
			var target = GetInputValueAsFloat("Target", Target);


			var force = SpringStrength*(target - previous) - Damping*speed;
			speed += force*Time.deltaTime;
			speed = Mathf.Sign(speed) * Mathf.Min(Mathf.Abs(speed), MaxSpeed);
			var value = previous + speed*Mathf.Min(GameTime.Delta30Fps, Time.deltaTime);

			_blackboard.SetValue(_speed, speed);
			_blackboard.SetValue(_value, value);
		}

		public override float GetValueAsFloat(NodePort port)
		{
			return _blackboard.GetValue(_value);
		}
	}
}