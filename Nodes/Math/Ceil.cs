using System;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing.Math
{
	[Serializable]
	public class Ceil : Node
	{
		[Input] public float Value;
		[Output] public float Output;

		public override float GetValueAsFloat(NodePort port)
		{
			return Mathf.Ceil(GetInputValueAsFloat("Value", Value));
		}
	}
}