﻿using Siccity.XNode;

namespace Flax.AnimatorProcessing.Math
{
    [System.Serializable]
    public class MathNode : Node {
        [Input] public float A;
        [Input] public float B;
        [Output] public float Output;

        public MathType mathType = MathType.Add;
        public enum MathType { Add, Subtract, Multiply, Divide }

		public override float GetValueAsFloat(NodePort port)
		{

			float a = GetInputValueAsFloat("A", A);
			float b = GetInputValueAsFloat("B", B);

            var output = 0f;
            if (port.fieldName == "Output")
                switch (mathType) {
                    case MathType.Add: default: output = a+b; break;
                    case MathType.Subtract: output = a - b; break;
                    case MathType.Multiply: output = a * b; break;
                    case MathType.Divide: output = a / b; break;
                }
            return output;
        }
    }
}