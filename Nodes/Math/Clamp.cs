﻿using System;
using Assets.Source.ExternalCode;
using Siccity.XNode;

namespace Flax.AnimatorProcessing.Math
{
	[Serializable]
	public class Clamp : Node
	{
		[Input] public float Value;
		[Output] public float Output;

		public MinMaxRange ValidRange = new MinMaxRange { rangeStart = 0, rangeEnd = 100 };

		public override float GetValueAsFloat(NodePort port)
		{
			var value = GetInputValueAsFloat("Value", Value);
			return ValidRange.Clamp(value);
		}
	}
}