﻿using System;
using Siccity.XNode;
using UnityEngine;

namespace Flax.AnimatorProcessing.Math
{
	[Serializable]
	public class Round : Node
	{
		[Input] public float Value;
		[Output] public float Output;

		public override float GetValueAsFloat(NodePort port)
		{
			return Mathf.Round(GetInputValueAsFloat("Value", Value));
		}
	}
}