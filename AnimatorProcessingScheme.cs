﻿using System.Collections.Generic;
using Siccity.XNode;
using UnityEngine;

namespace Assets.Source.Animation.InputProcessing
{
	[CreateAssetMenu]
	public class AnimatorProcessingScheme : NodeGraph
	{
		private List<INeedBlackboard> _needsBlackboard;
		private List<INeedAnimator> _needsAnimator;
		private List<IEndPoint> _endPoints;

		public AnimatorProcessingUnit CreateProcessingUnit()
		{
			Populate();
			var animatorProcessingState = new AnimatorProcessingUnit();
			animatorProcessingState.Initialise(this);
			return animatorProcessingState;
		}

		private void Populate()
		{
			_needsBlackboard = FindBlackboardNodes();
			_needsAnimator = FindAnimatorNodes();
			_endPoints = FindEndPointNodes();
		}

		private List<INeedBlackboard> FindBlackboardNodes()
		{
			var list = new List<INeedBlackboard>();
			foreach (var node in nodes)
			{
				if (!(node is INeedBlackboard)) continue;
				list.Add((INeedBlackboard)node);
			}

			return list;
		}

		private List<INeedAnimator> FindAnimatorNodes()
		{
			var list = new List<INeedAnimator>();
			foreach (var node in nodes)
			{
				if (!(node is INeedAnimator)) continue;
				list.Add((INeedAnimator)node);
			}

			return list;
		}

		private List<IEndPoint> FindEndPointNodes()
		{
			var list = new List<IEndPoint>();
			foreach (var node in nodes)
			{
				if (!(node is IEndPoint)) continue;
				list.Add((IEndPoint)node);
			}

			return list;
		}

		public void ProcessValues(AnimatorProcessingUnit unit)
		{
			foreach (var needAnimator in _needsAnimator)
			{
				needAnimator.GiveAnimatorAccess(unit);
			}

			foreach (var needBlackboard in _needsBlackboard)
			{
				needBlackboard.GiveBlackboardAccess(unit);
				needBlackboard.Update();
			}

			foreach (var endPoint in _endPoints)
			{
				endPoint.TriggerEndPoint();
			}
		}
	}
}
