﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Source.Animation.InputProcessing
{
	public class AnimatorProcessingUnit : IBlackboardAccess, IAnimatorAccess
	{
		private AnimatorProcessingScheme _scheme;
		private Animator _sourceAnimator;
		private Animator _destinationAnimator;

		private Dictionary<Guid, float> _blackboard = new Dictionary<Guid, float>();

		public void Initialise(AnimatorProcessingScheme scheme)
		{
			_scheme = scheme;
			_blackboard.Clear();
		}

		public void Update(Animator source, Animator destination)
		{
			_sourceAnimator = source;
			_destinationAnimator = destination;
			_scheme.ProcessValues(this);
		}

		public void SetValue(Guid parameter, float value)
		{
			_blackboard[parameter] = value;
		}

		public float GetValue(Guid parameter)
		{
			if (!_blackboard.ContainsKey(parameter)) _blackboard[parameter] = 0;
			return _blackboard[parameter];
		}

		public Animator GetSourceAnimator()
		{
			return _sourceAnimator;
		}

		public Animator GetDestinationAnimator()
		{
			return _destinationAnimator;
		}
	}

	public interface IAnimatorAccess
	{
		Animator GetSourceAnimator();
		Animator GetDestinationAnimator();
	}

	public interface IBlackboardAccess
	{
		void SetValue(Guid parameter, float value);
		float GetValue(Guid parameter);
	}
}